# GiveInKind SPA

This is a SPA that uses the next.js framework to support statically rendered html.

[Changelog](./changelog.md)

## Docs

- [Configuration](./docs/configuration.md)
- [Dependencies](./docs/dependencies.md)
- [Styling](./docs/styling.md)
- [Yarn](./docs/yarn.md)

## Setup project

Run the `init.js` script to perform all the needed steps automatically.

```
./init.sh
```

Or run the following steps manually:

**Copy .env-example to .env**

```sh
cp .env-example .env
```

**Run a yarn install**

Make sure to run an yarn install before continuing ^^

```sh
yarn install
```

Yarn workspaces is used and it will install all dependencies of the app and the individual packages found in the `packages` directory.

## Git

This repository is configured with git pre-commit hooks that will run when code is commited or pushed.

The hooks will run all configured tests and linters, if any of the tests fail a commit or push will be prevented.

This will work out of the box after the first `yarn install`.

## Adding dependencies

The `yarn add` command cannot be used because we are referencing internal packages within our package.json files which yarn cannot resolve by itself.

The recommended way at the moment is to manually add the dependency to the package.json and lookup the version on the npm website.
Afterwards just run a `yarn install` to install the dependency.

NOTE: [Lerna](https://github.com/lerna/lerna) can also be used to handle this scenario.

## Commands

### Start the development server

```sh
yarn start
```

### Start the development server in debug mode

This starts the server with the node debugger running on port 9229.

```sh
yarn run debug
```

### Run Linters

Run all configured linters.

```sh
yarn run lint
```

### Run Tests

Run all configured tests (including linters).  
Note: This command is used with git pre-commit hooks.

```sh
yarn run test
```

### Build for production

This will perform a webpack build put all the bundled files under the `.next` directory.

```sh
yarn run build
```

### Create static files

This will build static html files and copy all needed assets into the `out` directory.

```sh
yarn run export
```

### Run an express server that will serve up production files

This is useful for testing the production build (Make sure to build and export first).

```sh
yarn run serve
```
