const fs = require('fs');
const { COPYFILE_EXCL } = fs.constants;

try {
  fs.copyFileSync('.env-example', '.env', COPYFILE_EXCL);
} catch (e) {
  console.info('Skipping copying of .env-example: .env already exists.');
}
