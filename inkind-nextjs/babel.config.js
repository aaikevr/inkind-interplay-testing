module.exports = api => {
  api.cache(true);

  return {
    presets: [
      [
        'next/babel',
        {
          'preset-env': {
            targets: {
              node: 'current',
              ie: '11',
            },
            modules: false,
            corejs: '3',
            useBuiltIns: 'usage',
          },
        },
      ],
    ],
  };
};
