/* eslint @typescript-eslint/no-var-requires: 0 */
const express = require('express');
const next = require('next');
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();
const yargs = require('yargs');

const argv = yargs
  .option('port', {
    alias: 'p',
    description: 'Server port',
    type: 'number',
    default: '3000',
  })
  .option('host', {
    alias: 'h',
    description: 'Server host',
    type: 'string',
    default: '0.0.0.0',
  }).argv;

nextApp
  .prepare()
  .then(() => {
    const app = express();

    // use gzip compression
    // app.use(compression());

    // server.use('/', express.static('./static_root'))

    // let next handle all other requests
    app.get('*', (req, res) => {
      return handle(req, res);
    });

    // Start the server
    let server = app.listen(argv.port, argv.host, err => {
      if (err) throw err;
      console.log(`Development server is running at http://${server.address().address}:${server.address().port}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
