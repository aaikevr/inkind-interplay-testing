// TODO those types can easily be changed into enums do we can easily use the combination of value + type enum provides to create better self-generating stories

export type UIVariant = 'default' | 'primary' | 'secondary';
export type UIButtonSpecialVariants = 'amazon' | 'paypal';
export type UIButtonVariant =
  | UIVariant
  | UIButtonSpecialVariants
  | 'default-light'
  | 'default-outline'
  | 'default-link'
  | 'default-plain'
  | 'primary-outline'
  | 'primary-link'
  | 'primary-plain'
  | 'primary-inverted'
  | 'secondary-outline'
  | 'secondary-link'
  | 'secondary-plain'
  | 'primary-light'
  | 'primary-light-outline'
  | 'primary-light-link'
  | 'primary-light-plain'
  | 'primary-dark'
  | 'primary-dark-outline'
  | 'primary-dark-link'
  | 'primary-dark-plain'
  | 'primary-vdark'
  | 'white'
  | 'white-link'
  | 'danger'
  | 'danger-outline'
  | 'danger-link'
  | 'danger-plain'
  | 'warning'
  | 'warning-outline'
  | 'warning-link'
  | 'warning-plain'
  | 'success'
  | 'success-outline'
  | 'success-link'
  | 'success-plain'
  | 'deluxe'
  | 'deluxe-outline';

export type UICheckboxVariant = UIVariant | 'white';
export type UISwitchVariant = UIVariant | 'white';

export type UISelectVariant =
  | UIVariant
  | 'default-solid'
  | 'primary-solid'
  | 'secondary-solid'
  | 'danger'
  | 'danger-solid'
  | 'warn'
  | 'warn-solid'
  | 'success'
  | 'success-solid';

export type UIBoxVariant = UIVariant | 'white' | 'love';
export type UIBoxDecoration = 'foliage-small-green';
export type UISectionDecoration = 'foliage' | 'foliage-pointy' | 'flowers' | 'foliage-round' | 'foliage-small';

export type HTMLButtonType = 'button' | 'reset' | 'submit';
export type UIButtonType = HTMLButtonType | 'a';
export type UIContextVariant = 'error' | 'warning' | 'success';
export type UISize = 'xs' | 'sm' | 'base' | 'lg' | 'xl' | '2xl' | '3xl';
export type IconSize = UISize;
export type UINotificationVariant = 'default' | 'blue' | 'purple' | 'green' | 'lightblue' | UIContextVariant;

export type UIGradient = 'aqua' | 'dark-aqua' | 'light-aqua' | 'love' | 'warm' | 'purps' | 'neutral' | 'sage';

// TODO: the home page also use this new color coding for features but this was setup manually
export enum UIFeaturesType {
  CareCalendar = 'primary',
  Wishlist = 'danger',
  Donations = 'success',
  Updates = 'secondary',
}
