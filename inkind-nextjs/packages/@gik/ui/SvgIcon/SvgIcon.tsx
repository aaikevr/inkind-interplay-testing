import React, { HTMLAttributes } from 'react';
import { IconSize } from '../types';
import * as HeroIcons from 'heroicons-react';
import tailwindConfig from '@/../tailwind.config';
import bemBlock from '@gik/core/utils/bemBlock';

interface BaseSvgIconProps extends HTMLAttributes<HTMLElement> {
  name: string;
  size?: IconSize;
  set?: SvgSet;
}

type HeroIconsList = string;
interface HeroSvgIconProps extends BaseSvgIconProps {
  name: HeroIconsList;
}

export type SvgIconProps = HeroSvgIconProps;

const heroIconsSet = 'hero-icons';
const gikIconsSet = 'gik-icons';
type HeroIconsSet = typeof heroIconsSet;
type GikIconsSet = typeof gikIconsSet;
export type SvgSet = HeroIconsSet | GikIconsSet;

const svgSets = {
  [heroIconsSet]: HeroIcons,
};

const blockName = `svg-icon`;

export default function SvgIcon({
  name,
  size = 'base',
  className,
  set = heroIconsSet,
  ...otherProps
}: SvgIconProps): React.ReactElement {
  const bem = bemBlock(blockName);

  const notFoundStr = '⛔';

  const selectedSet = svgSets[set];
  const Value = selectedSet[name as string];

  if (!Value) {
    return (
      <i className={blockName} title={`SvgIcon has no mapping: ${name}`}>
        {notFoundStr}
      </i>
    );
  }

  return (
    <Value
      className={bem(
        null,
        [
          {
            [set]: true,
          },
          {
            [name as string]: true,
          },
        ],
        className
      )}
      size={tailwindConfig.theme.iconSize[size]}
      {...otherProps}
    />
  );
}
