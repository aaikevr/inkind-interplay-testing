import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const VisitorsDisabled = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M19.8031 11.3284C20.5532 10.5783 20.9747 9.56087 20.9747 8.5C20.9747 7.43913 20.5532 6.42172 19.8031 5.67157C19.053 4.92143 18.0355 4.5 16.9747 4.5C15.9138 4.5 14.8964 4.92143 14.1462 5.67157C13.3961 6.42172 12.9747 7.43913 12.9747 8.5C12.9747 9.56087 13.3961 10.5783 14.1462 11.3284C14.8964 12.0786 15.9138 12.5 16.9747 12.5C18.0355 12.5 19.053 12.0786 19.8031 11.3284Z"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M20.9978 17.8333V28.4999H12.9978L12.9978 15.8333"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M4.9978 4.5C4.9978 12.5 10.9978 15.8333 16.9978 16.5C22.9978 17.1667 28.9978 18.5 28.9978 28.5"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinecap="round"
        />
        <path d="M28.9978 4.5L4.9978 28.5" stroke="#EF5C84" strokeWidth="2" strokeLinecap="round" />
      </svg>
    );
  }
);
