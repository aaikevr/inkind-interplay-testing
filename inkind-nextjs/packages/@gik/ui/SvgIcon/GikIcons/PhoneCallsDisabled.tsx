import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const PhoneCallsDisabled = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M5.31242 5.28105C4.81232 5.78115 4.53137 6.45942 4.53137 7.16667V8.5C4.53137 19.5453 13.486 28.5 24.5314 28.5H25.8647C26.572 28.5 27.2502 28.219 27.7503 27.719C28.2504 27.2189 28.5314 26.5406 28.5314 25.8333V21.4613C28.5314 21.1814 28.4434 20.9085 28.2797 20.6814C28.116 20.4543 27.885 20.2845 27.6194 20.196L21.6287 18.1987C21.3243 18.0975 20.9935 18.1094 20.6972 18.2324C20.4008 18.3553 20.1588 18.581 20.0154 18.868L18.5087 21.8773C15.244 20.4022 12.6291 17.7874 11.154 14.5227L14.1634 13.016C14.4504 12.8726 14.6761 12.6305 14.799 12.3342C14.9219 12.0379 14.9339 11.7071 14.8327 11.4027L12.8354 5.412C12.747 5.14661 12.5774 4.91574 12.3505 4.75207C12.1237 4.5884 11.8511 4.50021 11.5714 4.5H7.19804C6.49079 4.5 5.81252 4.78095 5.31242 5.28105Z"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M28.5314 4.5L4.53137 28.5" stroke="#EF5C84" strokeWidth="2" strokeLinecap="round" />
      </svg>
    );
  }
);
