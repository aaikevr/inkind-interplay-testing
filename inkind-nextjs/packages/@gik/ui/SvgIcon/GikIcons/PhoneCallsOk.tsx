import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const PhoneCallsOk = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M5.65605 5.28105C5.15595 5.78115 4.875 6.45942 4.875 7.16667V8.5C4.875 19.5453 13.8297 28.5 24.875 28.5H26.2083C26.9156 28.5 27.5939 28.219 28.094 27.719C28.594 27.2189 28.875 26.5406 28.875 25.8333V21.4613C28.8751 21.1814 28.787 20.9085 28.6233 20.6814C28.4596 20.4543 28.2286 20.2845 27.963 20.196L21.9723 18.1987C21.6679 18.0975 21.3371 18.1094 21.0408 18.2324C20.7445 18.3553 20.5024 18.581 20.359 18.868L18.8523 21.8773C15.5876 20.4022 12.9728 17.7874 11.4977 14.5227L14.507 13.016C14.794 12.8726 15.0197 12.6305 15.1426 12.3342C15.2656 12.0379 15.2775 11.7071 15.1763 11.4027L13.179 5.412C13.0906 5.14661 12.921 4.91574 12.6941 4.75207C12.4673 4.5884 12.1947 4.50021 11.915 4.5H7.54167C6.83442 4.5 6.15615 4.78095 5.65605 5.28105Z"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    );
  }
);
