import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const FlowersOk = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M16.4785 11.709C17.4791 10.6715 18.536 9.38873 20.1996 8.68084C23.0724 7.45842 26.6179 7.16675 26.6179 7.16675C26.6179 7.16675 27.6917 12.0169 24.5087 15.2504C21.3257 18.484 16.1255 17.7654 16.1255 17.7654"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M14.9995 10.0142C12.0847 7.16675 5.76933 7.16675 5.76933 7.16675C5.76933 7.16675 4.31193 12.048 7.71253 15.3023C11.1131 18.5566 17.4285 17.743 17.4285 17.743C17.4285 17.743 17.9143 12.8617 14.9995 10.0142Z"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M20.1255 8.4999C18.7922 5.16659 16.1256 4.49998 16.1256 4.49998C16.1256 4.49998 13.4589 5.16659 12.1256 8.49996"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M10.8922 21.1667C13.2022 21.2767 14.7922 23.1666 14.7922 23.1666C14.7922 23.1666 13.1921 25.1667 10.7922 25.1667C8.39229 25.1667 6.79245 23.1666 6.79245 23.1666C6.79245 23.1666 8.58222 21.0566 10.8922 21.1667Z"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M17.4589 17.8334C17.4589 17.8334 16.6296 16.6663 16.135 23.4883C15.9788 25.6432 17.7937 27.1794 19.0609 27.8525C21.8057 29.3101 25.4589 27.8525 25.4589 27.8525"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    );
  }
);
