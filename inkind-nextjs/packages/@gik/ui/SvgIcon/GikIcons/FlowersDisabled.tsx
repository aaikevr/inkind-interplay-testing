import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const FlowersDisabled = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M17.3508 11.709C18.3513 10.6715 19.4082 9.38873 21.0719 8.68084C23.9447 7.45842 27.4902 7.16675 27.4902 7.16675C27.4902 7.16675 28.5639 12.0169 25.3809 15.2504C22.198 18.484 16.9978 17.7654 16.9978 17.7654"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M15.8718 10.0142C12.957 7.16675 6.64157 7.16675 6.64157 7.16675C6.64157 7.16675 5.18417 12.048 8.58477 15.3023C11.9854 18.5566 18.3008 17.743 18.3008 17.743C18.3008 17.743 18.7866 12.8617 15.8718 10.0142Z"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M20.9978 8.4999C19.6645 5.16659 16.9979 4.49998 16.9979 4.49998C16.9979 4.49998 14.3312 5.16659 12.9979 8.49996"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M11.7645 21.1667C14.0745 21.2767 15.6645 23.1666 15.6645 23.1666C15.6645 23.1666 14.0644 25.1667 11.6645 25.1667C9.26456 25.1667 7.66472 23.1666 7.66472 23.1666C7.66472 23.1666 9.45449 21.0566 11.7645 21.1667Z"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinejoin="round"
        />
        <path
          d="M18.3311 17.8334C18.3311 17.8334 17.5019 16.6663 17.0073 23.4883C16.851 25.6432 18.6659 27.1794 19.9332 27.8525C22.6779 29.3101 26.3311 27.8525 26.3311 27.8525"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M28.9978 4.5L4.9978 28.5" stroke="#EF5C84" strokeWidth="2" strokeLinecap="round" />
      </svg>
    );
  }
);
