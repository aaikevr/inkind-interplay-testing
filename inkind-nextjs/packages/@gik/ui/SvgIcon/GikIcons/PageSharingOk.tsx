import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const PageSharingOk = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M10.9789 13.8333H8.31224C7.60499 13.8333 6.92671 14.1143 6.42662 14.6144C5.92652 15.1145 5.64557 15.7928 5.64557 16.5L5.64557 25.8333C5.64557 26.5406 5.92652 27.2189 6.42662 27.719C6.92671 28.219 7.60499 28.5 8.31224 28.5L24.3122 28.5C25.0195 28.5 25.6978 28.219 26.1979 27.719C26.698 27.2189 26.9789 26.5406 26.9789 25.8333L26.9789 16.5C26.9789 15.7928 26.698 15.1145 26.1979 14.6144C25.6978 14.1143 25.0195 13.8333 24.3122 13.8333H21.6456M20.3122 8.5L16.3122 4.5M16.3122 4.5L12.3122 8.5M16.3122 4.5V21.8333"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    );
  }
);
