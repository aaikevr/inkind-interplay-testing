import React from 'react';

export interface GikIconProps extends React.SVGProps<SVGSVGElement> {
  size?: number;
}

export const GikIconDefaultSize = 24;

export { default as ShareAlt } from './ShareAlt';
export { default as Messenger } from './Messenger';
export { default as Facebook } from './Facebook';
export { default as Twitter } from './Twitter';
export { default as YouTube } from './YouTube';
export { default as Pinterest } from './Pinterest';
export { default as Instagram } from './Instagram';
export * from './Eat';
export * from './Reminder';
export * from './Fundraising';
export * from './Calendar';
export * from './Wishlist';
export * from './WishlistLarge';
export * from './PayPal';
export * from './GoFundMe';
export * from './InterrogationRed';
export * from './Play';
export * from './PhoneCallsOk';
export * from './PhoneCallsDisabled';
export * from './FlowersOk';
export * from './FlowersDisabled';
export * from './TextMessagesOk';
export * from './TextMessagesDisabled';
export * from './VisitorsOk';
export * from './VisitorsDisabled';
export * from './PageSharingOk';
export * from './PageSharingDisabled';
export * from './Features';
