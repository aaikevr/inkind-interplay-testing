import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const PageSharingDisabled = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M11.6644 13.8333H8.99778C8.29054 13.8333 7.61226 14.1143 7.11216 14.6144C6.61207 15.1145 6.33112 15.7928 6.33112 16.5L6.33112 25.8333C6.33112 26.5406 6.61207 27.2189 7.11216 27.719C7.61226 28.219 8.29054 28.5 8.99778 28.5L24.9978 28.5C25.705 28.5 26.3833 28.219 26.8834 27.719C27.3835 27.2189 27.6644 26.5406 27.6644 25.8333L27.6644 16.5C27.6644 15.7928 27.3835 15.1145 26.8834 14.6144C26.3833 14.1143 25.705 13.8333 24.9978 13.8333H22.3311M20.9978 8.5L16.9978 4.5M16.9978 4.5L12.9978 8.5M16.9978 4.5V21.8333"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M28.9978 4.5L4.9978 28.5" stroke="#EF5C84" strokeWidth="2" strokeLinecap="round" />
      </svg>
    );
  }
);
