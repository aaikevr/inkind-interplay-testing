import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const VisitorsOk = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M18.9309 11.3284C19.681 10.5783 20.1024 9.56087 20.1024 8.5C20.1024 7.43913 19.681 6.42172 18.9309 5.67157C18.1807 4.92143 17.1633 4.5 16.1024 4.5C15.0416 4.5 14.0242 4.92143 13.274 5.67157C12.5239 6.42172 12.1024 7.43913 12.1024 8.5C12.1024 9.56087 12.5239 10.5783 13.274 11.3284C14.0242 12.0786 15.0416 12.5 16.1024 12.5C17.1633 12.5 18.1807 12.0786 18.9309 11.3284Z"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M20.1255 17.8333V28.4999H12.1255L12.1255 15.8333"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M4.12555 4.5C4.12555 12.5 10.1255 15.8333 16.1255 16.5C22.1255 17.1667 28.1256 18.5 28.1256 28.5"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinecap="round"
        />
      </svg>
    );
  }
);
