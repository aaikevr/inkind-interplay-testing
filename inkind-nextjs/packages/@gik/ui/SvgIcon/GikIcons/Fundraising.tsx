import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const Fundraising = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M10.6094 17.3315C11.1934 17.3315 10.8906 17.3315 12.0075 17.3315M12.0075 17.3315C14.4734 17.3315 19.0226 17.3314 19.8053 17.3315C20.9424 17.3315 21.3546 18.5148 20.3292 18.9034C19.7834 19.1102 18.4716 19.6909 17.3641 20.1888C16.1843 20.7191 14.9067 20.9994 13.6131 20.9994H9.34731C8.86418 20.9994 8.38281 20.941 7.91367 20.8256L6 20.3547V15.4179V15.4179C7.58874 14.0488 9.81251 13.8865 11.8512 14.3787C12.4552 14.5245 13.0136 14.5862 13.3889 14.471C14.6555 15.4179 13.9533 17.3007 12.0075 17.3315Z"
          stroke="#7BAB52"
          strokeWidth="2.18675"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M3 16.5C3 15.6716 3.67157 15 4.5 15H6V21H4.5C3.67157 21 3 20.3284 3 19.5V16.5Z"
          stroke="#7BAB52"
          strokeWidth="2.18675"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M16.458 4.17578C14.801 4.17578 13.458 5.07078 13.458 6.17578C13.458 7.28078 14.801 8.17578 16.458 8.17578C18.115 8.17578 19.458 9.07078 19.458 10.1758C19.458 11.2808 18.115 12.1758 16.458 12.1758M16.458 4.17578V12.1758M16.458 4.17578C17.568 4.17578 18.538 4.57778 19.057 5.17578M16.458 4.17578V3.17578M16.458 12.1758V13.1758M16.458 12.1758C15.348 12.1758 14.378 11.7738 13.859 11.1758"
          stroke="#7BAB52"
          strokeWidth="2.18675"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    );
  }
);
