import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '.';

export default React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        width={size}
        height={size}
        viewBox="0 0 24 24"
        fill="none"
        ref={svgRef}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M19.8137 5.42297C20.6743 5.65571 21.352 6.34147 21.582 7.21232C22 8.79064 22 12.0838 22 12.0838C22 12.0838 22 15.3769 21.582 16.9553C21.352 17.8261 20.6743 18.5119 19.8137 18.7448C18.2542 19.1676 12 19.1676 12 19.1676C12 19.1676 5.7458 19.1676 4.18614 18.7448C3.32557 18.5119 2.64784 17.8261 2.41784 16.9553C2 15.3769 2 12.0838 2 12.0838C2 12.0838 2 8.79064 2.41784 7.21232C2.64784 6.34147 3.32557 5.65571 4.18614 5.42297C5.7458 5 12 5 12 5C12 5 18.2542 5 19.8137 5.42297ZM9.95446 9.09386V15.0737L15.1817 12.0839L9.95446 9.09386Z"
          fill="#ff0000"
        />
      </svg>
    );
  }
);
