import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const TextMessagesDisabled = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M11.6645 13.8333H11.6778M16.9978 13.8333H17.0111M22.3311 13.8333H22.3445M12.9978 21.8333H7.66447C6.95723 21.8333 6.27895 21.5523 5.77885 21.0522C5.27875 20.5521 4.9978 19.8738 4.9978 19.1666V8.49992C4.9978 7.79267 5.27875 7.1144 5.77885 6.6143C6.27895 6.1142 6.95723 5.83325 7.66447 5.83325H26.3311C27.0384 5.83325 27.7167 6.1142 28.2168 6.6143C28.7169 7.1144 28.9978 7.79267 28.9978 8.49992V19.1666C28.9978 19.8738 28.7169 20.5521 28.2168 21.0522C27.7167 21.5523 27.0384 21.8333 26.3311 21.8333H19.6645L12.9978 28.4999V21.8333Z"
          stroke="#BCC7C8"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M28.9978 4.5L4.9978 28.5" stroke="#EF5C84" strokeWidth="2" strokeLinecap="round" />
      </svg>
    );
  }
);
