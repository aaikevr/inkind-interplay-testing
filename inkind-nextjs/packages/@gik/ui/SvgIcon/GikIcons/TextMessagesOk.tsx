import * as React from 'react';
import { GikIconDefaultSize, GikIconProps } from '@gik/ui/SvgIcon/GikIcons/index';

export const TextMessagesOk = React.forwardRef(
  ({ size = GikIconDefaultSize, ...props }: GikIconProps, svgRef: React.Ref<SVGSVGElement>) => {
    return (
      <svg
        viewBox="0 0 33 33"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        width={size}
        height={size}
        ref={svgRef}
        {...props}
      >
        <path
          d="M10.7922 13.8333H10.8055M16.1255 13.8333H16.1389M21.4589 13.8333H21.4722M12.1255 21.8333H6.79222C6.08497 21.8333 5.40669 21.5523 4.9066 21.0522C4.4065 20.5521 4.12555 19.8738 4.12555 19.1666V8.49992C4.12555 7.79267 4.4065 7.1144 4.9066 6.6143C5.40669 6.1142 6.08497 5.83325 6.79222 5.83325H25.4589C26.1661 5.83325 26.8444 6.1142 27.3445 6.6143C27.8446 7.1144 28.1256 7.79267 28.1256 8.49992V19.1666C28.1256 19.8738 27.8446 20.5521 27.3445 21.0522C26.8444 21.5523 26.1661 21.8333 25.4589 21.8333H18.7922L12.1255 28.4999V21.8333Z"
          stroke="#7BAB52"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    );
  }
);
