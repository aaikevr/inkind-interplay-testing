import React from 'react';
import classnames from 'classnames';
import {
  UIButtonVariant,
  UISize,
  UIButtonType,
  HTMLButtonType,
} from '../types';
import { gikClassPrefix } from '@gik/core/constants';

type ButtonElement = HTMLAnchorElement | HTMLInputElement | HTMLButtonElement;

export interface ButtonProps extends React.HTMLAttributes<ButtonElement> {
  /**
   * The type of button determines what tag should be used to render the button to the DOM.
   * available options: a, input, button, reset, submit
   */
  type?: UIButtonType;

  /**
   * If `type` is `a` then this controls whether to use an external link or not.
   * Defaults to `false`.
   */
  externalLink?: boolean;

  /**
   * If type is `a` then this controls whether to scroll to the component id
   */
  scrollLink?: boolean;

  /**
   * Adds a floating tooltip to the button
   */
  tooltip?: string;

  /**
   * Button variant to use
   */
  variant?: UIButtonVariant;

  /**
   * Size the button on a fixed scale
   */
  // see https://www.notion.so/Button-component-doesnt-support-size-2xl-2fe9bdf7beeb44f9b8e0122caa4e1024
  size?: UISize;

  /**
   * Content to be added behind the main (children) content
   */
  append?: React.ReactNode;

  /**
   * Content to be added before the main (children) content
   */
  prepend?: React.ReactNode;

  /**
   * tabIndex that is passed to the main DOM element.
   * Set this to -1 to exclude this element from the index
   *
   * @interplayignore
   */
  tabIndex?: number;

  /**
   * display button as a block (full-width)
   */
  block?: boolean;

  /**
   * display button as full-width
   */
  fullWidth?: boolean;

  /**
   * force display of hover state
   */
  hover?: boolean;

  /**
   * force display of active state
   */
  active?: boolean;

  /**
   * force display of focus state
   */
  focus?: boolean;

  /**
   * put button in disabled mode
   */
  disabled?: boolean;

  /**
   * put button in loading mode
   */
  loading?: boolean;

  /**
   * use rounded corners (default)
   */
  uppercase?: boolean;

  /**
   * use rounded corners (default)
   */
  rounded?: boolean;

  /**
   * use pill shaped corners
   */
  pill?: boolean;

  /**
   * use squared corners
   */
  squared?: boolean;

  /**
   * Adds extra padding in x axis
   */
  wide?: boolean;

  /**
   * Href attribute to add to anchor tags
   */
  href?: string;

  /**
   * Href target
   */
  target?: string;

  /**
   * rel attribute
   */
  rel?: string;

  /**
   * use circular shape (this will apply border-radius: 50% with a fixed width and height)
   */
  circle?: boolean;

  /**
   * Attach this to a form that isn't a parent of this element.
   */
  form?: string;

  preventClickWhenDisabled?: boolean;
}

const blockName = `${gikClassPrefix}-button`;

function Button(
  {
    type = 'a',
    externalLink = false,
    scrollLink = false,
    variant = 'primary',
    size = 'base',
    tabIndex = 0,
    block,
    append,
    prepend,
    children,
    focus,
    loading,
    className,
    squared,
    circle,
    pill,
    rounded,
    active,
    hover,
    disabled,
    uppercase = true,
    wide,
    onClick,
    onFocus,
    onBlur,
    onKeyPress,
    tooltip,
    href,
    fullWidth,
    preventClickWhenDisabled,
    ...otherProps
  }: ButtonProps,
  ref: React.LegacyRef<ButtonElement>
): React.ReactElement {
  const [focused, setFocused] = React.useState(false);
  const finalTabIndex = disabled ? -1 : tabIndex;

  let buttonRef: ButtonElement;

  function handleClick(ev: React.MouseEvent<ButtonElement>): void {
    // prevent clicking if button is disabled
    if (preventClickWhenDisabled && disabled) return;

    // trigger custom callback if provided
    if (onClick) {
      onClick(ev);
    }

    // don't bother with a focus rectangle if it wasn't focused already
    // TODO: this does not work correctly, need to handle this in the mouseDown and mouseUp events
    buttonRef?.blur && buttonRef.blur();
  }

  function handleKeyPress(ev: React.KeyboardEvent<ButtonElement>): void {
    if (ev.which === 32 || ev.which === 13) {
      // this will prevent scroll when the user uses spacebar to press the button
      ev.preventDefault();
      // trigger a click event when space or enter is pressed
      if (onClick) {
        // eslint-disable-next-line
        // @ts-ignore
        onClick(new MouseEvent('click'));
      }
      // trigger custom callback if provided
      if (onKeyPress) {
        onKeyPress(ev);
      }
    }
  }

  function handleFocus(ev: React.FocusEvent<ButtonElement>): void {
    // disabled buttons should not receive focus
    // note: onFocus should never be called in the first place
    // buttons in these states should have a tabIndex of -1
    if (disabled) {
      return;
    }

    setFocused(true);
    if (onFocus) {
      onFocus(ev);
    }
  }

  function handleBlur(ev: React.FocusEvent<ButtonElement>): void {
    setFocused(false);
    if (onBlur) {
      onBlur(ev);
    }
  }

  // generate classnames based on properties
  // TODO: this can be refactored in various ways...
  const classNames: string = classnames([
    { [blockName]: true },
    { [`${blockName}--${variant}`]: variant },
    { [`${blockName}--size-${size}`]: size },
    { [`${blockName}--active`]: active },
    { [`${blockName}--hover`]: hover },
    { [`${blockName}--block`]: block },
    { [`${blockName}--full-width`]: fullWidth },
    { [`${blockName}--disabled`]: disabled },
    { [`${blockName}--focus`]: focused || focus },
    { [`${blockName}--loading `]: loading },
    { [`${blockName}--pill `]: pill },
    { [`${blockName}--circle `]: circle },
    { [`${blockName}--squared `]: squared },
    { [`${blockName}--rounded `]: rounded },
    { [`${blockName}--wide `]: wide },
    { [`${blockName}--uppercase `]: uppercase },
    { ['is-text']: variant && variant.endsWith('-text') },
    className || '',
  ]);

  // always wrap content in a span to avoid plain text which can not be position correctly
  const newChildren: React.ReactNode = (
    <>
      {prepend}
      <span className={`${blockName}__content`}>{children}</span>
      {append}
    </>
  );

  const commonProps = {
    tabIndex: finalTabIndex,
    className: classNames,
    onClick: handleClick,
    onFocus: handleFocus,
    onBlur: handleBlur,
    onKeyPress: handleKeyPress,
    ...otherProps,
  };



  return (
    <button
      ref={(_ref: HTMLButtonElement) => {
        buttonRef = _ref;
        return ref;
      }}
      type={type as HTMLButtonType}
      {...commonProps}
    >
      {newChildren}
    </button>
  );

}

export default React.forwardRef(Button);
