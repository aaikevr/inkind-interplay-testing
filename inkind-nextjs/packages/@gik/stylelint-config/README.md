# Give Inkind Stylelint configuration

Extends [stylelint-config-recommended](https://github.com/stylelint/stylelint-config-recommended) and overrides some rules to suite our codebase.
