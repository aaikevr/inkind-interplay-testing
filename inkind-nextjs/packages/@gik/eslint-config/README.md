# Give Inkind ESlint Configuration

a Shared ESLint configuration used throughout the Give Inkind codebase.

## Usage

```sh
yarn add -D @gik/eslint-config
```

Then create a file named `.eslintrc` with following content in the root folder of your project:

```json
{
  "extends": "@gik/eslint-config"
}
```

Add this to the scripts section in your package.json to be able to run `yarn lint` to execute the linter.

```json
{
  "lint": "eslint ."
}
```

That's it! You can override the settings by editing your `.eslintrc` file. Learn more about [configuring ESLint](http://eslint.org/docs/user-guide/configuring) on the ESLint website.

Eslint Plugins used:

- [eslint:recommended](https://github.com/yannickcr/eslint-plugin-react)
- [@typescript-eslint/recommended](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin)
- [@typescript-eslint/eslint-recommended](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin)
- [react/recommended](https://github.com/yannickcr/eslint-plugin-react)
- [jest/recommended](https://github.com/jest-community/eslint-plugin-jest)
- [prettier](https://github.com/prettier/eslint-config-prettier)
- [prettier/@typescript-eslint](https://github.com/typescript-eslint/typescript-eslint)
- [stylelint-config-prettier](https://github.com/prettier/stylelint-config-prettier)
