module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es6: true,
    jest: true,
    'jest/globals': true,
  },
  plugins: ['@typescript-eslint', 'import', 'react', 'jsx-a11y', 'jest', 'prettier', 'use-effect-no-deps'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:jest/recommended',
    'prettier',
    'prettier/@typescript-eslint',
    'prettier/react',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  settings: {
    // https://github.com/yannickcr/eslint-plugin-react
    react: {
      createClass: 'createReactClass', // Regex for Component Factory to use,
      // default to "createReactClass"
      pragma: 'React', // Pragma to use, default to "React"
      version: 'detect', // React version. "detect" automatically picks the version you have installed.
      // You can also use `16.0`, `16.3`, etc, if you want to override the detected value.
      // default to latest and warns if missing
      // It will default to "detect" in the future
    },
    linkComponents: [
      // Components used as alternatives to <a> for linking, eg. <Link to={ url } />
      'Hyperlink',
      { name: 'Link', linkAttribute: 'to' },
    ],
  },
  rules: {
    // report prettier issues as errors
    'prettier/prettier': 'error',

    '@typescript-eslint/explicit-module-boundary-types': 0,
    '@typescript-eslint/no-empty-interface': 0,

    // allow ts-ignore
    '@typescript-eslint/ban-ts-comment': 0,

    // ----- React: https://github.com/yannickcr/eslint-plugin-react
    // disable the need for prop-types
    'react/prop-types': 0,
    // disable the need for a displayName
    'react/display-name': 0,

    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'use-effect-no-deps/use-effect-no-deps': 'error',

    // allow the use of types such as 'object' to conform with libraries such as react-final-form
    '@typescript-eslint/ban-types': 0,

    // arguments prefixed with _ are allowed to be unused: https://eslint.org/docs/rules/no-unused-vars#argsignorepattern
    '@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^_' }],

    // ----- JSX a11y: https://github.com/evcohen/eslint-plugin-jsx-a11y/tree/master/docs/rules
    // 'jsx-a11y/accessible-emoji': 'warn',
    // 'jsx-a11y/alt-text': 'warn',
    // 'jsx-a11y/anchor-has-content': 'warn',
    // 'jsx-a11y/aria-activedescendant-has-tabindex': 'warn',
    // 'jsx-a11y/aria-props': 'warn',
    // 'jsx-a11y/aria-proptypes': 'warn',
    // 'jsx-a11y/aria-role': 'warn',
    // 'jsx-a11y/aria-unsupported-elements': 'warn',
    // 'jsx-a11y/heading-has-content': 'warn',
    // 'jsx-a11y/href-no-hash': 'warn',
    // 'jsx-a11y/iframe-has-title': 'warn',
    // 'jsx-a11y/img-redundant-alt': 'warn',
    // 'jsx-a11y/no-access-key': 'warn',
    // 'jsx-a11y/no-distracting-elements': 'warn',
    // 'jsx-a11y/no-redundant-roles': 'warn',
    // 'jsx-a11y/role-has-required-aria-props': 'warn',
    // 'jsx-a11y/role-supports-aria-props': 'warn',
    // 'jsx-a11y/scope': 'warn',
  },
};
