import { gikClassPrefix } from '../constants';
import classnames from 'classnames';

export type bemFunction = (
  element?: string,
  modifiers?: string | string[] | object | object[],
  classNames?: string | string[] | object | object[]
) => string;

export default function bemBlock(blockName: string, prefix = `${gikClassPrefix}-`): bemFunction {
  return function bem(element?: string, modifiers?: string | string[], classNames?: string | string[]): string {
    const block = `${prefix}${blockName}`;

    const blockElement = element ? `${block}__${element}` : block;
    const blockModifiers = modifiers
      ? Array.isArray(modifiers)
        ? classnames(
            modifiers
              .filter(m => m)
              .map(m => {
                if (typeof m === 'string') {
                  return `${blockElement}--${m}`;
                } else {
                  const key = Object.keys(m)[0];
                  return { [`${blockElement}--${key}`]: m[key] };
                }
              })
          )
        : `${blockElement}--${modifiers}`
      : '';

    if (classNames) {
      if (Array.isArray(classNames)) {
        return classnames([blockElement, blockModifiers, ...classNames]);
      } else {
        return classnames([blockElement, blockModifiers, classNames]);
      }
    } else {
      return classnames([blockElement, blockModifiers]);
    }
  };
}
