export const gikClassPrefix = 'gik';

/**
 * Specifies if the http client should fetch urls through the cache server.
 */
export const useHttpCache = true;

export const gikHasOverlayClass = `${gikClassPrefix}-has-overlay`;

// NOTE: I'm not sure the deduping interval should be this high, we might have to experiment with this.
export const SWRLongCachingOptions = {
  dedupingInterval: 20000,
  focusThrottleInterval: 30000,
};

export const defaultUserImagePath = 'images?src=user/default';
