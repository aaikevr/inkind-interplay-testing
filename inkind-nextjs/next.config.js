/* eslint @typescript-eslint/no-var-requires: 0 */
const path = require('path');
const rehypePrism = require('@mapbox/rehype-prism');
const CopyFilePlugin = require('copy-webpack-plugin');
const withMDX = require('@next/mdx')({
  extension: /\.mdx?$/,
  options: {
    rehypePlugins: [rehypePrism],
  },
});

const rootPath = __dirname;
const pathToSrc = path.join(rootPath, 'src');
const pathToGik = path.join(rootPath, 'packages', '@gik');
const pathToCore = path.join(pathToGik, 'core');
const pathToHomepage = path.join(pathToGik, 'homepage');
const pathToAbout = path.join(pathToGik, 'about');

const withTM = require('next-transpile-modules')([
  // These packages needs to be transpiled to be sure they pass `es-check es5` (to make sure they can run on IE11)
  'ky',
  'zustand',
]);

const config = {
  pageExtensions: ['jsx', 'js', 'ts', 'tsx', 'mdx'],

  // https://nextjs.org/docs/api-reference/next.config.js/disabling-x-powered-by
  poweredByHeader: false,

  // gzip compression
  // https://nextjs.org/docs/api-reference/next.config.js/compression
  compress: true,

  devIndicators: {
    // https://nextjs.org/docs/api-reference/next.config.js/static-optimization-indicator
    autoPrerender: false,
  },

  // optional catch all routing for deeply nested dynamic routes
  // https://nextjs.org/docs/routing/dynamic-routes#optional-catch-all-routes
  optionalCatchAll: true,

  // pass environment variables through to nextjs app
  env: {
    BASE_URL: process.env.BASE_URL,
    WP_API_URL: process.env.WP_API_URL,
    WC_API_URL: process.env.WC_API_URL,
    DOTNET_API_URL: process.env.DOTNET_API_URL,
    API_CACHE_URL: process.env.API_CACHE_URL,
    STRIPE_API_KEY: process.env.STRIPE_API_KEY,
    ADGLARE_NAME: process.env.ADGLARE_NAME,
    ADGLARE_ZONES: process.env.ADGLARE_ZONES,
  },

  webpack(config, { dev }) {
    // important! always use the react and other core libs in local node_modules to avoid issues
    config.resolve.alias['react'] = path.join(__dirname, 'node_modules/react');
    config.resolve.alias['react-dom'] = path.join(__dirname, 'node_modules/react-dom');

    // ----- Configure aliases

    config.resolve.alias = Object.assign(config.resolve.alias || {}, {
      '@': pathToSrc,
      ui: path.join(pathToSrc, 'components', 'ui'),
      '@gik': pathToGik,
    });

    // ----- setup SVGR to import svg images as react components

    config.module.rules.push({
      test: /\.svg$/,
      use: [
        {
          loader: '@svgr/webpack',
          options: {
            svgoConfig: {
              plugins: {
                removeViewBox: false,
              },
            },
          },
        },
      ],
    });

    // ----- copy various assets to the public directory using WriteFilePlugin

    config.plugins.push(
      new CopyFilePlugin({
        patterns: [
          {
            from: path.join(pathToSrc, 'public'),
            to: path.join(rootPath, 'public'),
          },
          {
            from: path.join(pathToCore, 'assets', 'fonts'),
            to: path.join(rootPath, 'public', 'fonts'),
          },
          {
            from: path.join(pathToCore, 'assets', 'img'),
            to: path.join(rootPath, 'public', 'img'),
          },
          {
            from: path.join(pathToHomepage, 'assets', 'img'),
            to: path.join(rootPath, 'public', 'homepage', 'img'),
          },
          {
            from: path.join(pathToAbout, 'assets', 'img'),
            to: path.join(rootPath, 'public', 'about', 'img'),
          },
          {
            from: path.join(pathToCore, 'styles', 'prism'),
            to: path.join(rootPath, 'public', 'css', 'prism'),
          },
        ],
      })
    );

    if (dev) {
      config.devtool = 'eval-source-map';
    }

    // uncomment the following lines to export the generated webpack config to a file
    const fs = require('fs');
    fs.writeFileSync('config-export.webpack.json', JSON.stringify(config, null, 2));

    return config;
  },
};

module.exports = withTM(withMDX(config));
