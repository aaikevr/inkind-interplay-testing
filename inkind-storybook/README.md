## Give Inkind Storybook

This is the storybook for Give Inkind which covers all components and documents them.

## Setup project

Copy the `.env-example` file to `.env` and change the variables as needed.

Make sure to run an yarn install before continuing ^^

```sh
yarn install
```

Yarn workspaces is used and it will install all dependencies of the storybook and nextjs app as well as the individual packages found in the `inkind-nextjs/packages` directory.

## Adding dependencies

The `yarn add` command cannot be used because we are referencing internal packages within our package.json files which yarn cannot resolve by itself.

The recommended way at the moment is to manually add the dependency to the package.json and lookup the version on the npm website.
Afterwards just run a `yarn install` to install the dependency.

NOTE: [Lerna](https://github.com/lerna/lerna) can also be used to handle this scenario.

## Commands

### Start the storybook dev server

```sh
yarn start
```

### Run Tests

Run all configured tests (including linters).  
Note: This command is used with git pre-commit hooks.

```sh
yarn run test
```

Use the following script to make jest generate an output into a json file.

```sh
yarn run test:generate-output
```

### Build for production

Build static files for storybook in the `storybook-static` directory.

```sh
yarn run build
```
