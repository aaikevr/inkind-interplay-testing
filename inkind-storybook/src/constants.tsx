export const VARIANTS = ['default', 'primary', 'secondary'];
export const VARIANTS_CONTEXT = ['danger', 'warn', 'success'];
export const VARIANTS_NOTIFICATION = ['default', 'error', 'warning', 'success'];
export const VARIANTS_BOX = VARIANTS.concat('white', 'love');
export const VARIANTS_ALL = VARIANTS.concat(VARIANTS_CONTEXT);
export const TYPES = ['a', 'button', 'reset', 'submit'];
export const SIZES = ['xs', 'sm', 'base', 'lg', 'xl'];
export const LEADING_MAP = {
  1000: 'none',
  1250: 'tight',
  1375: 'snug',
  1500: 'normal',
  1625: 'relaxed',
  2000: 'loose',
};

export const WEIGHT_MAP = {
  '200': 'thin',
  '300': 'light',
  normal: 'normal',
  '500': 'medium',
  '600': 'semibold',
  bold: 'bold',
  '800': 'extrabold',
  '900': 'black',
};

export const FONT_SIZE_MAP = {
  750: 'xs',
  875: 'sm',
  1000: 'base',
  1125: 'lg',
  1250: 'xl',
  1500: '2xl',
  1875: '3xl',
  2250: '4xl',
  3000: '5xl',
  4000: '6xl',
};
