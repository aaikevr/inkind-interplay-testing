import React from 'react';
import Button from '@gik/ui/Button';

export default function ButtonOverview(): React.ReactElement {
  return (
    <div className="tw-grid tw-grid-cols-6 tw-gap-4 tw-mb-8">
      <div className="sb-grid-cell-header">&nbsp;</div>
      <div className="sb-grid-cell-header">Normal</div>
      <div className="sb-grid-cell-header">Hover</div>
      <div className="sb-grid-cell-header">Active</div>
      <div className="sb-grid-cell-header">Disabled</div>
      <div className="sb-grid-cell-header">Focus</div>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Primary</div>
      <Button variant="primary">Button</Button>
      <Button variant="primary" hover>
        Button
      </Button>
      <Button variant="primary" active>
        Button
      </Button>
      <Button variant="primary" disabled>
        Button
      </Button>
      <Button variant="primary" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Primary Outline</div>
      <Button variant="primary-outline">Button</Button>
      <Button variant="primary-outline" hover>
        Button
      </Button>
      <Button variant="primary-outline" active>
        Button
      </Button>
      <Button variant="primary-outline" disabled>
        Button
      </Button>
      <Button variant="primary-outline" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Primary Plain</div>
      <Button variant="primary-plain">Button</Button>
      <Button variant="primary-plain" hover>
        Button
      </Button>
      <Button variant="primary-plain" active>
        Button
      </Button>
      <Button variant="primary-plain" disabled>
        Button
      </Button>
      <Button variant="primary-plain" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Primary Link</div>
      <div className="tw-text-center">
        <Button variant="primary-link">Link</Button>
      </div>
      <div className="tw-text-center">
        <Button variant="primary-link" hover>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="primary-link" active>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="primary-link" disabled>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="primary-link" focus>
          Link
        </Button>
      </div>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Secondary</div>
      <Button variant="secondary">Button</Button>
      <Button variant="secondary" hover>
        Button
      </Button>
      <Button variant="secondary" active>
        Button
      </Button>
      <Button variant="secondary" disabled>
        Button
      </Button>
      <Button variant="secondary" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Secondary Outline</div>
      <Button variant="secondary-outline">Button</Button>
      <Button variant="secondary-outline" hover>
        Button
      </Button>
      <Button variant="secondary-outline" active>
        Button
      </Button>
      <Button variant="secondary-outline" disabled>
        Button
      </Button>
      <Button variant="secondary-outline" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Secondary Plain</div>
      <Button variant="secondary-plain">Button</Button>
      <Button variant="secondary-plain" hover>
        Button
      </Button>
      <Button variant="secondary-plain" active>
        Button
      </Button>
      <Button variant="secondary-plain" disabled>
        Button
      </Button>
      <Button variant="secondary-plain" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Secondary Link</div>
      <div className="tw-text-center">
        <Button variant="secondary-link">Link</Button>
      </div>
      <div className="tw-text-center">
        <Button variant="secondary-link" hover>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="secondary-link" active>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="secondary-link" disabled>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="secondary-link" focus>
          Link
        </Button>
      </div>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Default</div>
      <Button variant="default">Button</Button>
      <Button variant="default" hover>
        Button
      </Button>
      <Button variant="default" active>
        Button
      </Button>
      <Button variant="default" disabled>
        Button
      </Button>
      <Button variant="default" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Default Outline</div>
      <Button variant="default-outline">Button</Button>
      <Button variant="default-outline" hover>
        Button
      </Button>
      <Button variant="default-outline" active>
        Button
      </Button>
      <Button variant="default-outline" disabled>
        Button
      </Button>
      <Button variant="default-outline" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Default Plain</div>
      <Button variant="default-plain">Button</Button>
      <Button variant="default-plain" hover>
        Button
      </Button>
      <Button variant="default-plain" active>
        Button
      </Button>
      <Button variant="default-plain" disabled>
        Button
      </Button>
      <Button variant="default-plain" focus>
        Button
      </Button>

      <div className="sb-grid-cell-header sb-grid-cell-header--left">Default Link</div>
      <div className="tw-text-center">
        <Button variant="default-link">Link</Button>
      </div>
      <div className="tw-text-center">
        <Button variant="default-link" hover>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="default-link" active>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="default-link" disabled>
          Link
        </Button>
      </div>
      <div className="tw-text-center">
        <Button variant="default-link" focus>
          Link
        </Button>
      </div>
    </div>
  );
}
