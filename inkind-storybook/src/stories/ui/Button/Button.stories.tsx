import React from 'react';
import Button from '@gik/ui/Button';
import SvgIcon from '@gik/ui/SvgIcon';

export default {
  title: 'UI/Button',
  component: Button,
};

export const Emoji = () => (
  <Button>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
  </Button>
);

export const Primary = () => <Button variant="primary">Primary</Button>;

export const Secondary = () => <Button variant="secondary">Secondary</Button>;

export const SVGIcon = () => (
  <Button prepend={<SvgIcon name="ChevronRight" />}>
    Test with icon
  </Button>
);
