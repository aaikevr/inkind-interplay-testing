import React from 'react';
import SvgIcon, { SvgIconProps, svgSets } from '@gik/ui/SvgIcon';
import OverviewDoc from './SvgIcon.mdx';
import { select } from '@storybook/addon-knobs';
import { SIZES } from '@/constants';
import { UISize } from '@gik/ui/types';

export default {
  title: 'UI/SvgIcon',
  component: SvgIcon,
};

export const Overview = () => <OverviewDoc />;

export const Tester = (args: unknown) => {
  const knobs: SvgIconProps = {
    size: select('size', SIZES, 'base') as UISize,
    name: select('name', Object.keys(svgSets['hero-icons']), 'AcademicCap'),
  };

  return <SvgIcon {...knobs} {...args} />;
};
