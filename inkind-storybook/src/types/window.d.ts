// These are initialised in the MVC project

interface GTMDataLayer {
  push(event: { event: string }): void;
}

interface SegmentAnalytics {
  track(eventName: string, eventData: { [key: string]: string }): void;
}

interface FullStoryAnalytics {
  event(eventName: string, eventData: { [key: string]: string }): void;
  identify(userId: string, { displayName: string, email: string }): void;
}

type UserLeapSurveyApi = (functionName: string, eventName: string) => void;
type IterateSurveyApi = (functionName: string, eventName: string) => void;

// keep in sync with legacy nextjs and storybook definition
interface LegacyReactWindow extends Window {
  dataLayer: GTMDataLayer;
  analytics: SegmentAnalytics;
  FS: FullStoryAnalytics;
  notificationManager: NotificationManager;
  __root: {
    reactAppBaseUrl: string;
    userId: string;
    userFirstName: string;
    userLastName: string;
    LoginModalOpened: boolean;
    antiForgery: {
      cookieName: string;
      tokenValue: string;
    };
  };
  Sentry: Sentry;
  DD_LOGS: {
    logger: DataDogLogger;
  };
}

// These are initialised in the MVC project
interface NextJSReactWindow extends Window {
  dataLayer: GTMDataLayer;
  analytics: SegmentAnalytics;
  FS: FullStoryAnalytics;
  notificationManager: NotificationManager;
  __root: {
    reactAppBaseUrl: string;
  };
  Sentry: Sentry;
  DD_LOGS: {
    logger: DataDogLogger;
  };
}
