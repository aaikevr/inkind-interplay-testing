export function randomImage(key: string | number) {
  return 'https://source.unsplash.com/random?v=' + key;
}

export function tangoCardImage(key: string | number) {
  return `https://picsum.photos/id/${key}/300/200`;
}
