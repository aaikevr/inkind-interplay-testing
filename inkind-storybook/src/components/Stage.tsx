import React, { CSSProperties } from 'react';
import classnames from 'classnames';

export interface IStageProps {
  children?: React.ReactNode;
  header?: React.ReactNode;
  className?: string;
  style?: CSSProperties;
  styleMain?: CSSProperties;
  classNameMain?: string;
}

const blockName = 'sb-stage';

export default function Stage({ children, header, className, style, classNameMain, styleMain }: IStageProps) {
  const classNames = classnames([blockName, className || '']);

  const classNamesMain = classnames([classNameMain || '']);

  return (
    <div className={classNames} style={style}>
      {header && <header>{header}</header>}
      <main style={styleMain} className={classNamesMain}>
        {children}
      </main>
    </div>
  );
}
