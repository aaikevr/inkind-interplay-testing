import React from 'react';

interface AppWrapperProps {
  children: React.ReactNode;
}

export default function AppWrapper({ children }: AppWrapperProps): React.ReactElement {
  return <>{children}</>;
}
