import React from 'react';
import ReactDOM from 'react-dom';

export interface IHeadProps {
  children: React.ReactNode;
}

export default function Head({ children }: IHeadProps): React.ReactElement {
  return ReactDOM.createPortal(children, document.head);
}
