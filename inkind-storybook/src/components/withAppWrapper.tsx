import React from 'react';
import AppWrapper from './AppWrapper';

export default function withAppWrapper(Story) {
  return (
    <AppWrapper>
      <Story />
    </AppWrapper>
  );
}
