import React, { Children } from 'react';

export interface ILinkProps extends React.HTMLAttributes<HTMLAnchorElement> {
  href: string;
}

export default function Link({ children, href, ...otherProps }: ILinkProps): React.ReactElement {
  const child = Children.only(children) as React.ReactElement;
  otherProps.onClick = ev => {
    ev.preventDefault();
    alert('LINK HREF: ' + href);
  };

  return React.cloneElement(child, { href, ...otherProps });
}
