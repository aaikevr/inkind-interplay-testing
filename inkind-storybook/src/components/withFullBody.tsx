import React from 'react';

export default function withFullBody(Story) {
  return (
    <div className="sb-fullbody">
      <Story />
    </div>
  );
}
