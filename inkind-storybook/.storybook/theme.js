import { create } from '@storybook/theming/create';

export default create({
  base: process.env.THEME ? process.env.THEME : 'dark',
  brandTitle: 'Give InKind',
  brandImage: '/assets/giveinkind-logo.svg',
});
