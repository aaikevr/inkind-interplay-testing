const fs = require('fs');
const ncp = require('ncp').ncp;
const rimraf = require('rimraf');
const mkdirp = require('mkdirp');

// The 'concurrency limit' is an integer that represents
// how many pending file system requests ncp has at a time.
ncp.limit = 16;

function copy(source, destination) {
  if (!fs.existsSync(destination)) {
    mkdirp.sync(destination);
  }
  ncp(source, destination, function (err) {
    if (err) return console.error(err);
  });
}

function copyAssets() {
  rimraf.sync('public');
  copy('./src/public', './public/assets');
}

module.exports = {
  copy: copy,
  copyAssets: copyAssets,
};
