const path = require('path');

const rootPath = path.join(__dirname, '..');
const pathToSrc = path.join(rootPath, 'src');
const pathToNodeModules = path.join(rootPath, 'node_modules');
const pathToNextJs = path.join(rootPath, '..', 'inkind-nextjs');
const pathToGik = path.join(pathToNextJs, `packages`, `@gik`);
const CompressionPlugin = require('compression-webpack-plugin');
const { ESBuildPlugin, ESBuildMinifyPlugin } = require('esbuild-loader');


module.exports = {
  stories: [path.join(pathToSrc, `stories`, `**`, `*.stories.@(ts|tsx|mdx)`)].map(a => a.split('\\').join('/')),
  addons: [
    {
      name: '@storybook/addon-docs',
      options: {
        // The configureJSX option is useful when you're writing your docs in MDX and your
        // project's babel config isn't already set up to handle JSX files.
        // babelOptions is a way to further configure the babel processor when you're using configureJSX.
        configureJSX: true,
        sourceLoaderOptions: null,
      },
    },
    '@storybook/addon-knobs',
    '@storybook/addon-controls',
    '@storybook/addon-backgrounds',
    '@storybook/addon-viewport'
  ],
  output: {
    compress: process.env.COMPRESS,
  },
  webpackFinal: async (config, { configType }) => {
    try {
      // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
      // You can change the configuration based on that.
      // 'PRODUCTION' is used when building the static version of storybook.

      // ----- Javascript

      config.module.rules.push({
        test: /\.(js|jsx)$/,
        include: [rootPath, pathToNextJs, pathToNodeModules],
        use: [
          {
            loader: require.resolve('esbuild-loader'),
            options: {
              target: 'es2015',
            },
          },
          // DocGen support for react typescript
          // {
          //   loader: require.resolve('react-docgen-typescript-loader'),
          //   options: {
          //     // Provide the path to your tsconfig.json so that your stories can
          //     // display types from outside each individual story.
          //     tsconfigPath: path.resolve(rootPath, 'tsconfig.json'),
          //   },
          // },
        ],
      });

      // ----- TypeScript

      config.module.rules.push({
        test: /\.(ts|tsx)$/,
        include: [rootPath, pathToNextJs, pathToNodeModules],
        use: [
          process.env.ESBUILD === 'true'
            ? {
                loader: require.resolve('esbuild-loader'),
                options: {
                  loader: 'tsx',
                  target: 'es2015',
                },
              }
            : { loader: require.resolve('ts-loader') },
          // DocGen support for react typescript
          // {
          //   loader: require.resolve('react-docgen-typescript-loader'),
          //   options: {
          //     // Provide the path to your tsconfig.json so that your stories can
          //     // display types from outside each individual story.
          //     tsconfigPath: path.resolve(rootPath, 'tsconfig.json'),
          //   },
          // },
        ],
      });

      // ----- Sass

      config.module.rules.push({
        test: /\.(scss|sass)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              import: false,
              importLoaders: 1,
              localsConvention: 'asIs',
              modules: false,
              url: false,
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [require('tailwindcss'), require('autoprefixer')],
              sourceMap: true,
            },
          },
          // {
          //   loader: 'resolve-url-loader',
          //   options: {
          //     sourceMap: true,
          //   },
          // },
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                sourceMap: true,
              },
            },
          },
        ],
        include: [rootPath, pathToNextJs],
      });

      config.resolve.extensions.push('.ts', '.tsx', '.mdx');

      config.resolve.alias = Object.assign(config.resolve.alias || {}, {
        '@': pathToSrc,
        '@gik/nextjs': path.join(pathToNextJs, 'src'),
        '@gik': pathToGik,
      });

      // ----- SVGR

      // modify storybook's file-loader rule to avoid conflicts with svgr
      const fileLoaderRule = config.module.rules.find(rule => {
        if (Array.isArray(rule.test)) {
          return rule.test.find(subrule => subrule.test('.svg'));
        } else {
          return rule.test.test('.svg');
        }
      });
      if (fileLoaderRule) fileLoaderRule.exclude = /\.svg$/;

      // add rule for svgr
      config.module.rules.push({
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              svgoConfig: {
                plugins: {
                  removeViewBox: false,
                },
              },
            },
          },
        ],
      });

      config.plugins.push(new ESBuildPlugin());

      if (process.env.COMPRESS) {
        // also generate gzipped files
        config.plugins.push(new CompressionPlugin());
        config.optimization.minimize = true;
        config.optimization.minimizer = [
          new ESBuildMinifyPlugin({
            sourcemap: true,
          }),
        ];
      }

      config.devtool = 'eval';

      return config;
    } catch (err) {
      console.log('webpack config error', err);
      process.exit(1);
    }
  },
  typescript: {
    check: false,
    checkOptions: {},
    reactDocgen: 'none'
  },
};
