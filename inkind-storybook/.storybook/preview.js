import { addDecorator, addParameters } from '@storybook/react';
import withAppWrapper from '@/components/withAppWrapper';
import { MINIMAL_VIEWPORTS as defaultViewports } from '@storybook/addon-viewport';

const customViewports = {};

import '@/styles/index.scss';

addDecorator(withAppWrapper);

addParameters({
  actions: { argTypesRegex: '^on.*' },
  docs: {
    inlineStories: false,
  },
  // controls: { expanded: true },
  a11y: {
    // optional selector which element to inspect
    element: '#root',
    // axe-core configurationOptions (https://github.com/dequelabs/axe-core/blob/develop/doc/API.md#parameters-1)
    config: {},
    // axe-core optionsParameter (https://github.com/dequelabs/axe-core/blob/develop/doc/API.md#options-parameter)
    options: {},
    // optional flag to prevent the automatic check
    manual: true,
  },
  backgrounds: {
    default: 'light',
    values: [
      { name: 'light', value: '#fff' },
      { name: 'dark', value: '#222' },
      { name: 'primary', value: '#41cbce' },
      { name: 'secondary', value: '#ff9e5a' },
    ],
  },
  cssresources: [
    {
      id: `bluetheme`,
      code: `<style>body { background-color: lightblue; }</style>`,
      picked: false,
      hideCode: false,
    },
  ],
  viewport: {
    viewports: {
      ...defaultViewports,
      ...customViewports,
    },
    // let's not make this the default
    // you can uncomment it though when doing mobile testing
    // defaultViewport: 'Large mobile',
  },
});
