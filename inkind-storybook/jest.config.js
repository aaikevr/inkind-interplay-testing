module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  setupFiles: ['./setupTests.ts'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  transform: {
    '^.+\\.[tj]sx?$': 'babel-jest',
    '^.+\\.mdx$': '@storybook/addon-docs/jest-transform-mdx',
  },
  moduleNameMapper: {
    '^@gik/(.*)$': '<rootDir>/../inkind-nextjs/packages/@gik/$1',
    '^@gik/nextjs(.*)$': '<rootDir>/../inkind-nextjs/src/$1',
  },
};
