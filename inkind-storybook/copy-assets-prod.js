const copyAssets = require('./.storybook/copy-assets').copyAssets;
const copy = require('./.storybook/copy-assets').copy;

// copy assets from source locations
copyAssets();

// copy public dir to static build
copy('./public', './storybook-static');
