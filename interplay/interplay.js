const fs = require('fs');
const path = require('path');

const rootPath = path.join(__dirname, '..');
const pathToStorybook = path.join(rootPath, 'inkind-storybook');
const pathToNextJs = path.join(rootPath, 'inkind-nextjs');

module.exports = {
  modifiers: {
    webpackConfig: (config, webpack) => {
      const scssRules = config.module.rules.filter((rule) => {
        if (!Array.isArray(rule.use)) return false;

        return rule.use.find((item) => item.loader === 'sass-loader');
      });

      scssRules.forEach((rule) => {
        if (rule.resolve.extensions.indexOf('.scss') > -1) {
          rule.test = /\.scss$/;
        }
        if (rule.resolve.extensions.indexOf('.sass') > -1) {
          rule.test = /\.sass$/;
        }

        rule.include = [pathToStorybook, pathToNextJs];

        rule.use[1].options = {
          import: false,
          importLoaders: 1,
          localsConvention: 'asIs',
          modules: false,
          url: false,
          sourceMap: true,
        };
      });

      config.resolve.extensions.push('.mdx');

      // uncomment next line to export the generated webpack config to a file
      fs.writeFileSync(
        'config-export-interplay.webpack.json',
        JSON.stringify(config, null, 2)
      );

      return config;
    },
  },
};
